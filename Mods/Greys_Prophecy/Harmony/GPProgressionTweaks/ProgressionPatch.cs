﻿using DMT;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

public class ProgressionPatch
{
    [HarmonyPatch(typeof(Progression), "ResetProgression")]
    private class ProgressionResetProgression
    {
        public static bool Prefix(Progression __instance, ProgressionValue[] ___ProgressionValueQuickList, bool _resetBooks)
        {
            int num = 0;
            for (int index = 0; index < ___ProgressionValueQuickList.Length; ++index)
            {
                ProgressionValue progressionValueQuick = ___ProgressionValueQuickList[index];
                ProgressionClass progressionClass = progressionValueQuick.ProgressionClass;
                if (!progressionClass.IsBook || _resetBooks)
                {
                    if (progressionValueQuick.Level > 0)
                    {
                        for (int _level = 1; _level <= progressionValueQuick.Level; ++_level)
                            num += progressionClass.CalculatedCostForLevel(_level);
                        progressionValueQuick.Level = 0;
                    }
                }
            }
            __instance.SkillPoints += num;
            return false;
        }
    }
}
