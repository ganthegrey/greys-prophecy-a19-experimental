using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using DMT;
using HarmonyLib;
using UnityEngine;

public class GPRagdoll
{
    [HarmonyPatch (typeof (EModelBase), "DoRagdoll")]
    [HarmonyPatch (new Type[] { typeof (DamageResponse), typeof (float) })]
    private class EModelBaseDoRagdoll
    {
        public static bool Prefix (EModelBase __instance, Entity ___entity, DamageResponse dr, float stunTime = 999999f)
        {
            if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
            {
                return true;
            }

            float strength = (float) dr.Strength;
            DamageSource source = dr.Source;
            if (strength > 0f && source != null)
            {
                Vector3 forceVec = source.getDirection ();
                float num2;
                if (dr.HitBodyPart == EnumBodyPartHit.None)
                {
                    num2 = ___entity.rand.RandomRange (5f, 25f);
                }
                else
                {
                    num2 = ___entity.rand.RandomRange (-20f, 50f);
                    strength *= 0.5f;
                    if (source.damageType == EnumDamageTypes.Bashing)
                    {
                        strength *= 2.5f;
                    }
                    if (dr.Critical)
                    {
                        num2 += 25f;
                        strength *= 2f;
                    }
                    if (dr.HitBodyPart == EnumBodyPartHit.Head)
                    {
                        strength *= 0.45f;
                    }
                    strength = Utils.FastMin (20f + strength, 500000f);
                    forceVec *= strength;
                }
                Vector3 axis = Vector3.Cross (forceVec.normalized, Vector3.up);
                forceVec = Quaternion.AngleAxis (num2, axis) * forceVec;
                __instance.DoRagdoll (stunTime, dr.HitBodyPart, forceVec, source.getHitTransformPosition (), false);
            }
            else
            {
                __instance.DoRagdoll (stunTime, dr.HitBodyPart, Vector3.zero, Vector3.zero, false);
            }

            return false;
        }
    }
}