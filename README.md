# Greys Prophecy A19 Experimental
A 7 Days to Die mod created by GanTheGrey and SavageHarbinger.

Grey's Prophecy is a flavoring of the default game. The goal of the mod is to add fun stuff, and some humor, while keeping close to the vanilla formula.

Some of the bigger changes that you will encounter are:
- Perk System overhaul to a more class based system
- Separate repair kits/items are back
- Custom Zombies
- Mumpfy's zombie texture variations
- Bigger backpack
- Bigger stack sizes for most items
- Craftable weapon, tool, and gun parts
- 10 slot toolbelt - Courtesy of Khaine (DF creator)
- 2 new book sets
- 3 new vehicles
- New weapons
- Update to the new Writable storage box to be able to pick it up like a workstation

Special Thanks:
- The Guppy's Discord modding server for help with issues and ideas
- Xyth for the hard work getting the custom animation controller working!
- Khaine for the 10 slot toolbelt
- Mumpfy for the zombie texture variations
- UncleSamIAm, Shugg, NoxAegis, DragonHeadGaming, LoriannTucker, PayneStoneback, biggytoe, and Bluejoy for QA
- And of course TFP for creating this game in the first place!
