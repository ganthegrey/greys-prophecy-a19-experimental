﻿using DMT;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using UnityEngine;

public class EntityNPCPatch
{
    [HarmonyPatch(typeof(EntityNPC), "DamageEntity")]
    public class EntityNPCPatch_DamageEntity
    {
        public static bool Prefix(EntityNPC __instance, DamageSource _damageSource, int _strength, bool _criticalHit, float _impulseScale)
        {
            if (__instance.NPCInfo != null && __instance.NPCInfo.TraderID > 0)
            {
                __instance.MinEventContext.Other = (GameManager.Instance.World.GetEntity(_damageSource.getEntityId()) as EntityAlive);
                __instance.FireEvent(MinEventTypes.onOtherDamagedSelf);
                return false;
            }
            return true;
        }
    }
}
